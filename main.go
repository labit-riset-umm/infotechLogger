package infotechLogs

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"

	amqp "github.com/rabbitmq/amqp091-go"
)

type loggerService struct {
	username    string
	password    string
	host        string
	port        string
	channelName string
}

func NewInfotechRabbitMQLogger(username string,
	password string,
	host string,
	port string, channelName string) *loggerService {
	return &loggerService{
		username:    username,
		password:    password,
		host:        host,
		port:        port,
		channelName: channelName,
	}
}

type LogClaims struct {
	IAT int64
	ISS string
	UID uint
	RO  string
}
type ApiEndpointLog struct {
	Ip     string
	Path   string
	Method string
	Token  string
	Date   int
	UserID int
	Role   string
}
type AuthEndpointLog struct {
	Ip     string
	Path   string
	Client string
	Date   int
}

func GetLogSessionData(c *gin.Context) LogClaims {
	sess := sessions.Default(c)
	res := sess.Get("CU-ACCESS")
	// Type assertion to convert to infotechLogs.LogClaims
	newVar := fmt.Sprintf("%s", res)
	// Remove formatting placeholders
	newVar = strings.ReplaceAll(newVar, "%!s(int64=", "")
	newVar = strings.ReplaceAll(newVar, "%!s(uint=", "")
	newVar = strings.ReplaceAll(newVar, ")", "")

	// Split the string into parts
	re := regexp.MustCompile(`{(\d+) ([A-Z-]+) (\d+) ([A-Z]+)}`)

	match := re.FindStringSubmatch(newVar)
	if match == nil {
		fmt.Println("Invalid input")
		return LogClaims{}
	}

	value1, err := strconv.Atoi(match[1])
	if err != nil {
		return LogClaims{}
	}
	value2 := match[2]
	value3, err := strconv.Atoi(match[3])
	if err != nil {
		return LogClaims{}
	}
	value4 := match[4]

	result := LogClaims{
		IAT: int64(value1),
		ISS: value2,
		UID: uint(value3),
		RO:  value4,
	}

	return result

}

func GenerateAPILog(c *gin.Context) ApiEndpointLog {
	tok, err := c.Cookie("ifx-at")
	if err != nil {
		c.JSON(http.StatusBadRequest, fmt.Sprintf("Unable to write log, error : %v", err.Error()))
		c.Abort()
	}

	date := time.Now().Unix()
	accessData := GetLogSessionData(c)
	log := ApiEndpointLog{
		Ip:     c.ClientIP(),
		Path:   c.FullPath(),
		Method: c.Request.Method,
		Token:  tok,
		Date:   int(date),
		UserID: int(accessData.UID),
		Role:   accessData.RO,
	}

	return log
}

func GenerateAuthLog(c *gin.Context) AuthEndpointLog {
	tok := c.GetHeader("IFX-CLIENT")

	date := time.Now().Unix()

	log := AuthEndpointLog{
		Ip:     c.ClientIP(),
		Path:   c.FullPath(),
		Client: tok,
		Date:   int(date),
	}

	return log
}

func (ls *loggerService) Push(log interface{}) error {
	b, err := json.Marshal(log)
	if err != nil {
		return fmt.Errorf("json marshall error, with real error : %v", err)
	}

	// Establish connection to RabbitMQ server
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", ls.username, url.QueryEscape(ls.password), ls.host, ls.port))
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	// Create a channel
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	defer ch.Close()

	// Declare a queue
	queueName := ls.channelName
	_, err = ch.QueueDeclare(
		queueName,
		false, // durable
		false, // autoDelete
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		panic(err)
	}

	// Publish a message to the queue
	err = ch.Publish(
		"",        // exchange
		queueName, // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        b,
		},
	)
	if err != nil {
		panic(err)
	}

	fmt.Println("Message published:")

	return nil
}

func (ls *loggerService) GinForwardMiddleware(tp string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var log interface{}

		switch tp {
		case "AUTH":
			log = GenerateAuthLog(c)
			break
		case "API":
			log = GenerateAPILog(c)
			break
		}

		err := ls.Push(log)
		if err != nil {
			c.JSON(http.StatusBadRequest, fmt.Sprintf("Unable to write log, error : %v", err.Error()))
			c.Abort()
		}

		c.Next()
	}
}

func (ls *loggerService) GinForwardLog(handler func(c *gin.Context)) gin.HandlerFunc {
	return func(c *gin.Context) {
		tok, err := c.Cookie("IFX-ACCESS-TOKEN")
		if err != nil {
			c.JSON(http.StatusBadRequest, fmt.Sprintf("Unable to write log, error : %v", err.Error()))
		}

		log := ApiEndpointLog{
			Ip:     c.ClientIP(),
			Path:   c.FullPath(),
			Method: c.Request.Method,
			Token:  tok,
		}

		err = ls.Push(log)
		if err != nil {
			c.JSON(http.StatusBadRequest, fmt.Sprintf("Unable to write log, error : %v", err.Error()))
		}

		handler(c)
	}
}
